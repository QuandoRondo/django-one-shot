from django.urls import path
from .views import todo_list_list, todo_list_detail, todo_list_create, todo_list_update, TodoListDeleteView, TodoItemCreateView, TodoItemUpdateView

urlpatterns = [
    path('', todo_list_list, name='todo_list_list'),
    path('<int:id>/', todo_list_detail, name='todo_list_detail'),
    path('create/', todo_list_create, name='todo_list_create'),
    path('<int:id>/edit/', todo_list_update, name='todo_list_update'),
    path('<int:id>/delete/', TodoListDeleteView.as_view(), name='todo_list_delete'),
    path('items/create/', TodoItemCreateView.as_view(), name='todo_item_create'),
    path('items/<int:id>/edit/', TodoItemUpdateView.as_view(), name='todo_item_update'),
]
