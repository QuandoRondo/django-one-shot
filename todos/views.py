from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm
from django.views import View


def todo_list_list(request):
    lists = TodoList.objects.all()
    return render(request, 'todos/todo_list_list.html', {'lists': lists})


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    items = TodoItem.objects.filter(list=todolist)
    return render(request, 'todos/todo_list_detail.html', {'todolist': todolist, 'items': items})


def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect('todo_list_detail', id=todolist.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/todo_list_form.html', {'form': form})


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoListForm(instance=todolist)
    return render(request, 'todos/todo_list_update.html', {'form': form, 'todolist': todolist})



class TodoListDeleteView(View):
    template_name = 'todos/todo_list_delete.html'

    def get(self, request, *args, **kwargs):
        todolist = get_object_or_404(TodoList, id=self.kwargs['id'])
        return render(request, self.template_name, {'todolist': todolist})

    def post(self, request, *args, **kwargs):
        todolist = get_object_or_404(TodoList, id=self.kwargs['id'])
        todolist.delete()
        return redirect('todo_list_list')




class TodoItemCreateView(View):
    form_class = TodoItemForm
    template_name = 'todos/todo_item_create.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect('todo_list_detail', id=todo_item.list.id)
        return render(request, self.template_name, {'form': form})



class TodoItemUpdateView(View):
    template_name = 'todos/todo_item_update.html'

    def get(self, request, id):
        todo_item = get_object_or_404(TodoItem, id=id)
        form = TodoItemForm(instance=todo_item)
        return render(request, self.template_name, {'form': form, 'todo_item': todo_item})

    def post(self, request, id):
        todo_item = get_object_or_404(TodoItem, id=id)
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=todo_item.list.id)
        return render(request, self.template_name, {'form': form, 'todo_item': todo_item})
